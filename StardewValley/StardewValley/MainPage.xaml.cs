﻿using FormsToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace StardewValley
{
	public partial class MainPage : TabbedPage
	{
		public MainPage()
		{
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            MessagingService.Current.Subscribe<bool>("connectivityChanged", (args, connected) =>
            {
                if (connected)
                    DisplayAlert("Internet connection found.", "Wait for the application data to update.", "OK");
                else
                    DisplayAlert("Internet connection lost.", "Application data may not be up to date. Connect to a working network.", "OK");
            });
        }
	}
}
