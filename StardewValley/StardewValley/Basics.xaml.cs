﻿using FormsToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace StardewValley
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Basics : ContentPage
	{
		public Basics ()
		{
			InitializeComponent ();
            MessagingService.Current.Subscribe<bool>("connectivityChanged", (args, connected) =>
            {
                if (connected)
                    DisplayAlert("Internet connection found.", "Wait for the application data to update.", "OK");
                else
                    DisplayAlert("Internet connection lost.", "Application data may not be up to date. Connect to a working network.", "OK");
            });
        }

        async void Handle_GameplayClicked(object sender, System.EventArgs e)
        {
            bool usersResponse = await DisplayAlert("Festivals",
                         "Are you really sure you want to continue?",
                         "Bring it on!",
                         "Nah");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new Gameplay());
            }
        }
	}
}