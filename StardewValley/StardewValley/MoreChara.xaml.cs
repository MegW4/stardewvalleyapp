﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using StardewValley.Models;
using FormsToolkit;

namespace StardewValley
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreChara : ContentPage
	{
		public MoreChara ()
		{
			InitializeComponent ();
            MessagingService.Current.Subscribe<bool>("connectivityChanged", (args, connected) =>
            {
                if (connected)
                    DisplayAlert("Internet connection found.", "Wait for the application data to update.", "OK");
                else
                    DisplayAlert("Internet connection lost.", "Application data may not be up to date. Connect to a working network.", "OK");
            });
        }

        public MoreChara(Characters villager)
        {
            InitializeComponent();
            BindingContext =villager;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var button = (Button)sender;
            Characters villager = (Characters)button.CommandParameter;
            var uri = new Uri(villager.Url);
            Device.OpenUri(uri);
        }
    }
}