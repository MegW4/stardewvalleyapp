﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Plugin.Connectivity;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ModernHttpClient;
using Newtonsoft.Json;
using FormsToolkit;
using StardewValley.Models;
using System.Collections.ObjectModel;

namespace StardewValley
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Walmart : ContentPage
	{
		public Walmart ()
		{
			InitializeComponent ();
            MessagingService.Current.Subscribe<bool>("connectivityChanged", (args, connected) =>
            {
                if (connected)
                    DisplayAlert("Internet connection found.", "Wait for the application data to update.", "OK");
                else
                    DisplayAlert("Internet connection lost.", "Application data may not be up to date. Connect to a working network.", "OK");
            });
        }

        async void GetProduct_Clicked(object sender, System.EventArgs e)
        {
             HttpClient client = new HttpClient();
           // var client = new HttpClient(new NativeMessageHandler());

            var uri = new Uri(
            string.Format(
                $"http://api.walmartlabs.com/v1/stores?apiKey=d8ea7pa7znszyvyqmrr57xs7&zip=92532&format=json"));

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");

            HttpResponseMessage response = await client.SendAsync(request);
            ObservableCollection<Item> Cells = new ObservableCollection<Item>();
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var item = Item.FromJson(content);
                Cells = item;
                ItemCell.ItemsSource = Cells;
            }

        }

        void Handle_MoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var walmartMore = (Item)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreWal(walmartMore));
        }
    }
}