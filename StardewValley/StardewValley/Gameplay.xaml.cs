﻿using FormsToolkit;
using StardewValley.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace StardewValley
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Gameplay : ContentPage
	{
		public Gameplay ()
		{
			InitializeComponent ();
            MessagingService.Current.Subscribe<bool>("connectivityChanged", (args, connected) =>
            {
                if (connected)
                    DisplayAlert("Internet connection found.", "Wait for the application data to update.", "OK");
                else
                    DisplayAlert("Internet connection lost.", "Application data may not be up to date. Connect to a working network.", "OK");
            });

            BindingContext = new ViewPickerModel();
        }

    }
}