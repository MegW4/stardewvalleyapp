﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using StardewValley.Models;
using FormsToolkit;

namespace StardewValley
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Villagers : ContentPage
    {

        public Villagers()
        {
            InitializeComponent();
            MessagingService.Current.Subscribe<bool>("connectivityChanged", (args, connected) =>
            {
                if (connected)
                    DisplayAlert("Internet connection found.", "Wait for the application data to update.", "OK");
                else
                    DisplayAlert("Internet connection lost.", "Application data may not be up to date. Connect to a working network.", "OK");
            });
            PopulateCharacterList();
        }

        ObservableCollection<Characters> Cells = new ObservableCollection<Characters>();
        private void PopulateCharacterList()
        {

            var C1 = new Characters()
            {
                ImageText = "Penny",
                ShortDescription = "Penny is a villager who lives in Pelican Town. She's one of the twelve characters available to marry. Her trailer is just east of the center of town, west of the river.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/a/ab/Penny.png",
                Url = "https://stardewvalleywiki.com/Penny",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/9/90/Penny_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/4/4b/Penny%2C_harvey._elliot.png"
            };

            var C2 = new Characters()
            {
                ImageText = "Maru",
                ShortDescription = "Maru is a villager who lives in The Mountains north of Pelican Town. She's one of the twelve characters available to marry. She lives north of town with her family in a house attached to Robin's carpenter shop.In the Social Status menu, Maru's outfit will change to a nursing uniform when she is at her job at the clinic.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/f/f8/Maru.png",
                Url = "https://stardewvalleywiki.com/Maru",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/f/f5/Maru_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/6/64/Maru-gadget.jpg"
            };

            var C3 = new Characters()
            {
                ImageText = "Leah",
                ShortDescription = "Leah is a villager who lives in a small cottage outside Pelican Town. She's one of the twelve characters available to marry. She spends each morning sculpting inside her cottage. Her home opens at 10:00 AM, but players must first acquire two hearts in friendship before being allowed inside.The first time the player enters her cottage an event will be triggered, but only if she is at home at the time.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/e/e6/Leah.png",
                Url = "https://stardewvalleywiki.com/Leah",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/b/b1/Leah_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/5/51/Leah_Sculpture.jpg"
            };

            var C4 = new Characters()
            {
                ImageText = "Haley",
                ShortDescription = "Haley is a villager who lives in Pelican Town. She's one of the twelve characters available to marry.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/1/1b/Haley.png",
                Url = "https://stardewvalleywiki.com/Haley",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/b/ba/Haley_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/9/9b/Haley_between_2_palms.png"
            };

            var C5 = new Characters()
            {
                ImageText = "Emily",
                ShortDescription = "Emily is a villager who lives in Pelican Town. Her home is south of the town square, right next to Jodi's, at the address 2 Willow Lane. She works most evenings at The Stardrop Saloon starting at about 4:00 PM. Emily loves to make her own clothing, but fabric can be difficult to come by in town. Among her favorite gifts are cloth and wool.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/2/28/Emily.png",
                Url = "https://stardewvalleywiki.com/Emily",
                InsideImage = "https://vignette.wikia.nocookie.net/stardewvalley/images/1/1f/Emily%27s_room.png/revision/latest?cb=20160409201437",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/1/1b/Emily%27s_crystal_garden.PNG"
            };

            var C6 = new Characters()
            {
                ImageText = "Abigail",
                ShortDescription = "Abigail is a villager who lives at Pierre's General Store in Pelican Town. She is one of the twelve characters available to marry.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/8/88/Abigail.png",
                Url = "https://stardewvalleywiki.com/Abigail",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/c/cd/Abigail_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/0/03/Abigailflute.PNG"
            };

            var C7 = new Characters()
            {
                ImageText = "Shane",
                ShortDescription = "Shane is a villager in Pelican Town who is often rude and unhappy, and suffers from depression and alcohol dependence. However, his attitude starts to change towards any player who chooses to befriend him. He works at JojaMart most days between 9 AM and 5 PM, and after work he frequently spends his evenings in The Stardrop Saloon. He doesn't work on the weekend except on rainy days, and is frequently around the ranch. In the Social Status menu, Shane's outfit will change to a Joja Corporation uniform when he is at his job at JojaMart. He's one of the twelve characters available to marry.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/8/8b/Shane.png",
                Url = "https://stardewvalleywiki.com/Shane",
                InsideImage = "http://i.imgur.com/krIl3la.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/d/de/Shane_and_charly.png"
            };

            var C8 = new Characters()
            {
                ImageText = "Sebastian",
                ShortDescription = "Sebastian is a villager who lives in in The Mountains north of Pelican Town. He's one of the twelve characters available to marry. Sebastian lives in the basement of his mother Robin's carpenter shop, north of town.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/a/a8/Sebastian.png",
                Url = "https://stardewvalleywiki.com/Sebastian",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/9/93/Sebastian_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/d/d1/Sebastian_Bike.png"
            };

            var C9 = new Characters()
            {
                ImageText = "Sam",
                ShortDescription = "Sam is a villager who lives in Pelican Town. He's one of the twelve characters available to marry. He lives in the southern part of town, just north of the river at 1 Willow Lane. Sam is an outgoing and energetic young man with a passion for music.He works part - time at JojaMart.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/9/94/Sam.png",
                Url = "https://stardewvalleywiki.com/Sam",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/f/fe/Sam_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/2/2c/Sampipe.jpg"
            };

            var C10 = new Characters()
            {
                ImageText = "Harvey",
                ShortDescription = "Harvey is a villager who lives in Pelican Town. He runs the town's medical clinic and is passionate about the health of the townsfolk. He's one of the twelve characters available to marry.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/9/95/Harvey.png",
                Url = "https://stardewvalleywiki.com/Harvey",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/f/ff/Harvey_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/4/4b/Penny%2C_harvey._elliot.png"
            };

            var C11 = new Characters()
            {
                ImageText = "Elliott",
                ShortDescription = "Elliott is a villager who lives on the beach south of Pelican Town. He's one of the twelve characters available to marry.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/b/bd/Elliott.png",
                Url = "https://stardewvalleywiki.com/Elliott",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/7/7d/Elliott_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/4/4b/Penny%2C_harvey._elliot.png"
            };

            var C12 = new Characters()
            {
                ImageText = "Alex",
                ShortDescription = "Alex is a villager who lives in the house southeast of Pierre's General Store. Alex is one of the twelve characters available to marry.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/0/04/Alex.png",
                Url = "https://stardewvalleywiki.com/Alex",
                InsideImage = "https://stardewvalleywiki.com/mediawiki/images/0/0a/Alex_Spouse_Room.png",
                OutsideImage = "https://stardewvalleywiki.com/mediawiki/images/7/7f/Alex_Workout_Area.jpg"
            };

            Cells.Add(C1);
            Cells.Add(C2);
            Cells.Add(C3);
            Cells.Add(C4);
            Cells.Add(C5);
            Cells.Add(C6);
            Cells.Add(C7);
            Cells.Add(C8);
            Cells.Add(C9);
            Cells.Add(C10);
            Cells.Add(C11);
            Cells.Add(C12);
            VillageCell.ItemsSource = Cells;
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            PopulateCharacterList();// Do whatever refresh logic you want here

            // Remember you have to set IsRefreshing False
            VillageCell.IsRefreshing = false;

        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Characters itemTapped = (Characters)listView.SelectedItem;
            var uri = new Uri(itemTapped.Url);
            Device.OpenUri(uri);
        }

        void Handle_MoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var villager = (Characters)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreChara(villager));
        }

        public void Handle_DeleteButton(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var villager = (Characters)menuItem.CommandParameter;

            int i = 0;
            int CountL = 0;
            while (Cells[i].ImageText != villager.ImageText)
            {
                i++;
                CountL++;
            }
            Cells.RemoveAt(CountL);          
        }
    }
}
