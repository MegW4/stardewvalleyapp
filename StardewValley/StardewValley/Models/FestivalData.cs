﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StardewValley.Models
{
    public  class FestivalData
    {
        public static IList<Festival> Festivals { get; private set; }

        static FestivalData()
        {
            Festivals = new List<Festival>();
            Festivals.Add(new Festival
            {
                Name = "Egg Festival",
                Year = "In Spring",
                Details = "The Egg Festival takes place on the 13th of every Spring. You enter the festival by entering Pelican Town Square between 9am and 2pm. When the festival ends, you will be returned to The Farm at 10pm. The featured attraction of the Egg Festival is the Egg Hunt.You and the other villagers search for small colored eggs hidden around Pelican Town.The Egg Hunt starts by talking to Mayor Lewis.You must gather 9 colored eggs(in 50 seconds, real time), or else Abigail will win.If you win, you will receive a Straw Hat as a reward.If you have already won an Egg Hunt in previous years, you will receive 1,000g. There is a booth set up where you can purchase a decorative Plush Bunny and Strawberry Seeds. (If you plant the seeds the same night as the festival, you will be able to harvest Strawberries two times before Summer). The festival ends after Mayor Lewis announces the winner of the Egg Hunt.",
                ImageUrl = "https://stardewvalleywiki.com/mediawiki/images/thumb/2/20/Egg_Festival.png/500px-Egg_Festival.png",
            });
            Festivals.Add(new Festival
            {
                Name = "Flower Dance",
                Year = "In Spring",
                Details = "The Flower Dance event takes place on the 24th of every Spring. You enter the dance by entering Cindersap Forest between 9am and 2pm. When the festival ends, you will be returned to The Farm at 10pm. The dance takes place in the far west of the forest, across the bridge that is south of the Wizard's Tower. This area is accessible only during the Flower Dance. There is a booth set up where you can purchase a Dandelion, Daffodil, Tub o' Flowers, and Rarecrow #5. During this festival, you can dance with one of the bachelors or bachelorettes.You must talk to them(twice) and invite them to dance, but they will refuse unless you have four hearts of friendship with them.You can talk to the other villagers and they will have event-related dialogue. Dancing will increase friendship with your partner by 1 heart (250 points). Once you talk to Mayor Lewis, the dance begins.The festival ends after the dance is performed.",
                ImageUrl = "https://stardewvalleywiki.com/mediawiki/images/thumb/a/ad/Flower_Festival.jpg/500px-Flower_Festival.jpg",
            });
            Festivals.Add(new Festival
            {
                Name = "Luau",
                Year = "In Summer",
                Details = "The Luau takes place on the 11th of every Summer. You attend the Luau by entering The Beach between 9am and 2pm. When the Luau ends, you will be returned to The Farm at 10pm. Pierre does not sell items from a booth at the Luau. A central feature of the Luau is the potluck soup.Villagers bring different ingredients to prepare this soup for the Governor, who tastes and judges it.The soup - tasting begins by talking to Mayor Lewis. Depending upon what type of produce you supply for the soup, there will be different reactions from the Governor and Mayor Lewis.The outcome will increase or reduce friendship points with all villagers. The Luau ends automatically after the Governor tastes the soup.",
                ImageUrl = "https://stardewvalleywiki.com/mediawiki/images/thumb/a/af/Luau.png/500px-Luau.png",
            });
            Festivals.Add(new Festival
            {
                Name = "Dance of the Moonlight Jellies",
                Year = "In Summer",
                Details = "The Dance of the Moonlight Jellies takes place on the 28th of every Summer. You enter the festival by entering The Beach between 10pm and 12am. When the festival ends, you will be returned to The Farm at 12am. This festival does not have a booth. During the festival, villagers gather at the docks to watch migrating jellyfish who are attracted to the light of a torch that Mayor Lewis lights.Talk to Mayor Lewis to begin the festival. The festival ends automatically after watching the jellyfish.",
                ImageUrl = "https://stardewvalleywiki.com/mediawiki/images/thumb/3/35/Dance_Of_The_Moonlight_Jellies.jpg/400px-Dance_Of_The_Moonlight_Jellies.jpg",
            });
            Festivals.Add(new Festival
            {
                Name = "Stardew Valley Fair",
                Year = "In Fall",
                Details = "The Stardew Valley Fair takes place on the 16th of Fall every year. You enter the fair by entering Pelican Town between 9am and 3pm. When you leave the festival, you will be returned to The Farm at 10pm. There are several games where you can gain Star Tokens, a currency that can be exchanged for prizes at the Shop near the entrance to the Bus Stop. The Fair also has a Grange Display contest where you can show the products of the farm and gain Star Tokens.Talk to Mayor Lewis when you're ready to have the products judged. Items displayed in the Grange Display are returned, but not automatically. During the Fair, you can eat Survival Burgers cooked by Gus north of Pierre's General Store for free. The festival ends when you leave Pelican Town.",
                ImageUrl = "https://stardewvalleywiki.com/mediawiki/images/thumb/4/45/StardewValleyFair.png/500px-StardewValleyFair.png",
            });
            Festivals.Add(new Festival
            {
                Name = "Spirit's Eve",
                Year = "In Fall",
                Details = "The Spirit's Eve festival takes place on the 27th of Fall every year. You enter the festival by entering Pelican Town between 10pm and 11:50pm. When you leave the festival, you will be returned to The Farm at 12am. The festival features a maze where it is possible to obtain the Golden Pumpkin.Pierre has a shopping booth where you can purchase festive items including a Rarecrow #2, a Jack-O-Lantern, and the Jack-O-Lantern recipe. The maze is located to the north of Pierre's festive shop. The maze has few dead ends and progresses in a nearly linear fashion. You have as much time as you need to navigate to the finish. Towards the end of the maze, in the northwest corner of the map, you must walk through a block of hedges to the left of a wooden sign with a question mark. Once through, you must go north through an open cavern and continue east to get to the Golden Pumpkin. The festival ends when you leave Pelican Town.",
                ImageUrl = "https://stardewvalleywiki.com/mediawiki/images/thumb/a/a9/Spirits_Eve.png/500px-Spirits_Eve.png",
            });
            Festivals.Add(new Festival
            {
                Name = "Festival of Ice",
                Year = "In Winter",
                Details = "The Festival of Ice takes place on the 8th of every Winter. You enter the festival by entering Cindersap Forest between 9am and 2pm. When the festival ends, you will be returned to The Farm at 10pm.At the festival there are ice sculptures and igloos set up.There's also an Ice Fishing Contest you can take part in along with Pam, Willy, and Elliott. Getting at least five fish in the first year will win you three tackles and a Sailor's Cap.In subsequent years, winning the contest will award 2,000g. The festival ends after Mayor Lewis announces the winner of the Ice Fishing contest.",
                ImageUrl = "https://stardewvalleywiki.com/mediawiki/images/thumb/4/48/Festival_of_Ice_2.jpg/400px-Festival_of_Ice_2.jpg",
            });
            Festivals.Add(new Festival
            {
                Name = "Feast of the Winter Star",
                Year = "In Winter",
                Details = "The Feast of the Winter Star takes place on the 25th of every Winter. You attend the festival by entering Pelican Town between 9am and 2pm. When the festival ends, you will be returned to The Farm at 10pm. The main event of the Feast of the Winter Star is secret gift-giving.A random villager will be selected to give you a gift, and you are assigned a random villager to give a gift to.A week beforehand, on the 18th, Mayor Lewis will send a letter telling you who your gift recipient is. You find out who is giving you a gift at the festival. The festival ends when you leave Pelican Town.",
                ImageUrl = "https://stardewvalleywiki.com/mediawiki/images/thumb/a/ae/Feast_of_the_Winterstar.png/500px-Feast_of_the_Winterstar.png",
            });
        }
    }
}
