﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace StardewValley.Models
{
    public class Crops
    {
        public string ImageText { get; set; }
        public string IconSource { get; set; }
        public string ShortDescription { get; set; }
        public string Url { get; set; }
        public string Recipe { get; set; }
        public string RecipeDesc { get; set; }      
    }
}
