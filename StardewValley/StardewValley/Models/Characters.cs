﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace StardewValley.Models
{
    public class Characters
    {
        public string ImageText { get; set; }
        public string IconSource { get; set; }
        public string ShortDescription { get; set; }
        public string Url { get; set; }
        public string InsideImage { get; set; }
        public string OutsideImage { get; set; }
    }
}
