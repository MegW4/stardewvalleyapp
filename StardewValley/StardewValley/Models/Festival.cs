﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StardewValley.Models
{
    public class Festival
    {
        public string Name { get; set; }
        public string Year { get; set; }
        public string Details { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
    }
}

