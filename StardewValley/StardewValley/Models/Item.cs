﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using StardewValley.Models;
//
//    var item = Item.FromJson(jsonString);
using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace StardewValley.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Item
    {
        [JsonProperty("no")]
        public long No { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("coordinates")]
        public ObservableCollection<double> Coordinates { get; set; }

        [JsonProperty("streetAddress")]
        public string StreetAddress { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("stateProvCode")]
        public string StateProvCode { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("sundayOpen")]
        public bool SundayOpen { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }
    }

    public partial class Item
    {
        public static ObservableCollection<Item> FromJson(string json) => JsonConvert.DeserializeObject<ObservableCollection<Item>>(json, StardewValley.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this ObservableCollection<Item> self) => JsonConvert.SerializeObject(self, StardewValley.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}