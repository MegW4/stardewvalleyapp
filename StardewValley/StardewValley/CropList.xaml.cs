﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using StardewValley.Models;
using XFGloss;
using System.Collections.ObjectModel;
using FormsToolkit;

namespace StardewValley
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CropList : ContentPage
	{
        
		public CropList ()
		{
			InitializeComponent ();
            PopulateCropList();
            MessagingService.Current.Subscribe<bool>("connectivityChanged", (args, connected) =>
            {
                if (connected)
                    DisplayAlert("Internet connection found.", "Wait for the application data to update.", "OK");
                else
                    DisplayAlert("Internet connection lost.", "Application data may not be up to date. Connect to a working network.", "OK");
            });

        }

        ObservableCollection<Crops> Cells = new ObservableCollection<Crops>();
        private void PopulateCropList()
        {
            var C1 = new Crops()
            {
                ImageText = "Cauliflower",
                ShortDescription = "Cauliflower is a vegetable crop that grows from Cauliflower Seeds after 12 days. Cauliflower is one of only three crops that can grow into a giant crop. This product is best sold with Artisan Profession as a pickled product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/a/aa/Cauliflower.png",
                Url = "https://stardewvalleywiki.com/Cauliflower",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/6/6e/Cheese_Cauliflower.png",
                RecipeDesc = "One of the recipes used in the game using Cauliflower is Cheese Cauliflower."
            };

            var C2 = new Crops()
            {
                ImageText = "Parsnip",
                ShortDescription = "The Parsnip is a vegetable crop that grows from Parsnip Seeds after 4 days. This product is best sold with Artisan Profession as a pickled product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/d/db/Parsnip.png",
                Url = "https://stardewvalleywiki.com/Parsnip",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/7/76/Parsnip_Soup.png",
                RecipeDesc = "One of the recipes used in the game using Parsnip is Parsnip Soup."
            };

            var C3 = new Crops()
            {
                ImageText = "Strawberry",
                ShortDescription = "The Strawberry is a fruit crop that grows from Strawberry Seeds after 8 days. This product is best sold with Artisan Profession as a iriduim quality wine product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/6/6d/Strawberry.png",
                Url = "https://stardewvalleywiki.com/Strawberry",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/6/6d/Strawberry.png",
                RecipeDesc = "One of the few crops with no recipe and is just eaten on its own. However it can be made into a jelly that sell for very well."
            };

            var C4 = new Crops()
            {
                ImageText = "Corn",
                ShortDescription = "Corn is a vegetable crop that grows from Corn Seeds after 14 days. This product is best sold with Artisan Profession as a pickled product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/f/f8/Corn.png",
                Url = "https://stardewvalleywiki.com/Corn",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/d/d7/Tortilla.png",
                RecipeDesc = "One of the recipes used in the game using Corn is Tortilla."
            };

            var C5 = new Crops()
            {
                ImageText = "Melon",
                ShortDescription = "The Melon is a fruit crop that grows from Melon Seeds after 12 days. Melon is one of only three crops that can grow into a giant crop. This product is best sold with Artisan Profession as a iriduim quality wine product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/1/19/Melon.png",
                Url = "https://stardewvalleywiki.com/Melon",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/3/32/Pink_Cake.png",
                RecipeDesc = "One of the recipes used in the game using Melon is Pink Cake."
            };

            var C6 = new Crops()
            {
                ImageText = "Starfruit",
                ShortDescription = "Starfruit is a fruit crop that grows from Starfruit Seeds after 13 days. Starfruit produces Artisan Goods that have some of the highest sell values in the game. This product is best sold with Artisan Profession as a iriduim quality wine product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/d/db/Starfruit.png",
                Url = "https://stardewvalleywiki.com/Starfruit",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/thumb/7/73/Junimo_Hut.png/72px-Junimo_Hut.png",
                RecipeDesc = "Like the strawberry it isn't used in any recipes but is used to make a building called Junimo Hut which is helpful late game."
            };

            var C7 = new Crops()
            {
                ImageText = "Tomato",
                ShortDescription = "The Tomato is a vegetable crop that grows from Tomato Seeds after 11 days. This product is best sold with Artisan Profession as a pickled product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/9/9d/Tomato.png",
                Url = "https://stardewvalleywiki.com/Tomato",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/f/f4/Pizza.png",
                RecipeDesc = "One of the recipes used in the game using Tomato is Pizza! Yay!"
            };

            var C8 = new Crops()
            {
                ImageText = "Amaranth",
                ShortDescription = "Amaranth is a vegetable crop that grows from Amaranth Seeds after 7 days. A Scythe is required to harvest Amaranth. This product is best sold with Artisan Profession as a pickled product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/f/f6/Amaranth.png",
                Url = "https://stardewvalleywiki.com/Amaranth",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/8/8b/Salmon_Dinner.png",
                RecipeDesc = "One of the recipes used in the game using Amaranth is Salmon Dinner."
            };

            var C9 = new Crops()
            {
                ImageText = "Eggplant",
                ShortDescription = "Eggplant is a vegetable crop that grows from Eggplant Seeds after 5 days. This product is best sold with Artisan Profession as a pickled product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/8/8f/Eggplant.png",
                Url = "https://stardewvalleywiki.com/Eggplant",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/8/87/Survival_Burger.png",
                RecipeDesc = "One of the recipes used in the game using Eggplant is Survival Burger."
            };

            var C10 = new Crops()
            {
                ImageText = "Pumpkin",
                ShortDescription = "The Pumpkin is a vegetable crop that grows from Pumpkin Seeds after 13 days. It is one of three crops which might produce a giant crop, along with Cauliflower and Melon. After Starfruit it has the second-highest per unit base price of all the normal crops. This product is best sold with Artisan Profession as a juice product.",
                IconSource = "https://stardewvalleywiki.com/mediawiki/images/6/64/Pumpkin.png",
                Url = "https://stardewvalleywiki.com/Pumpkin",
                Recipe = "https://stardewvalleywiki.com/mediawiki/images/7/7d/Pumpkin_Pie.png",
                RecipeDesc = "One of the recipes used in the game using Pumpkin is Pumpkin Pie."
            };

            Cells.Add(C1);
            Cells.Add(C2);
            Cells.Add(C3);
            Cells.Add(C4);
            Cells.Add(C5);
            Cells.Add(C6);
            Cells.Add(C7);
            Cells.Add(C8);
            Cells.Add(C9);
            Cells.Add(C10);
            CropCell.ItemsSource = Cells;

        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            PopulateCropList();// Do whatever refresh logic you want here

            // Remember you have to set IsRefreshing False
            CropCell.IsRefreshing = false;

        }

        void Handle_MoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var crops = (Crops)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreCrop(crops));
        }

        public void Handle_DeleteButton(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var crop = (Crops)menuItem.CommandParameter;

            int i = 0;
            int CountL = 0;
            while (Cells[i].ImageText != crop.ImageText)
            {
                i++;
                CountL++;
            }
            Cells.RemoveAt(CountL);
        }
    }
}