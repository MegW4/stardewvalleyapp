﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using StardewValley.Models;
using FormsToolkit;

namespace StardewValley
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreCrop : ContentPage
	{
		public MoreCrop ()
		{
			InitializeComponent ();
            MessagingService.Current.Subscribe<bool>("connectivityChanged", (args, connected) =>
            {
                if (connected)
                    DisplayAlert("Internet connection found.", "Wait for the application data to update.", "OK");
                else
                    DisplayAlert("Internet connection lost.", "Application data may not be up to date. Connect to a working network.", "OK");
            });
        }

        public MoreCrop(Crops crop)
        {
            InitializeComponent();
            BindingContext = crop;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var button = (Button)sender;
            Crops crop = (Crops)button.CommandParameter;
            var uri = new Uri(crop.Url);
            Device.OpenUri(uri);
        }
    }
}