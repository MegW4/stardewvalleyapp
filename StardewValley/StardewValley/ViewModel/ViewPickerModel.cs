﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StardewValley.Models
{
    public class ViewPickerModel : ViewPicker
    {
        public IList<Festival> Festivals { get { return FestivalData.Festivals; } }

        Festival selectedFestival;
        public Festival SelectedFestival
        {
            get { return selectedFestival; }
            set
            {
                if (selectedFestival != value)
                {
                    selectedFestival = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
